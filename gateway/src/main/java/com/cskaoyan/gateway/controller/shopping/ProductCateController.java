package com.cskaoyan.gateway.controller.shopping;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.IProductCateService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dto.AllProductCateRequest;
import com.mall.shopping.dto.AllProductCateResponse;
import com.mall.shopping.dto.ProductCateDto;
import com.mall.user.annotation.Anoymous;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: ZaiRiChangYe
 * @Date: 2020/5/13 11:17
 * @Description: 获取所有产品分类中心控制层
 */
@RestController
@RequestMapping("/shopping")
public class ProductCateController {

    @Reference(check = false)
    private IProductCateService productCateService;

    @GetMapping("/categories")
    @Anoymous
    public ResponseData allCategories(AllProductCateRequest request){
        AllProductCateResponse allProductCate = productCateService.getAllProductCate(request);
        List<ProductCateDto> productCateDtoList = allProductCate.getProductCateDtoList();
        // 如果allProductCate为空
        if (CollectionUtils.isEmpty(productCateDtoList)){
            return new ResponseUtil<>().setErrorMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        return new ResponseUtil<>().setData(productCateDtoList);
    }
}
