package com.cskaoyan.gateway.controller.user;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.user.IUserVerifyService;
import com.mall.user.annotation.Anoymous;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.dto.UserVerifyRequest;
import com.mall.user.dto.UserVerifyResponse;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author: ZaiRiChangYe
 * @Date: 2020/5/12 11:41
 * @Description: 用户验证中心控制层
 */
@RestController
@RequestMapping("/user")
public class UserVerifyController {

    @Reference(check = false)
    private IUserVerifyService userVerifyService;

    @GetMapping("/verify")
    @Anoymous // 因为在user路径下，加上注解后取消不必要的拦截 -- TokenInterceptor进行放行
    public ResponseData verify(@RequestParam String uid, @RequestParam String username, HttpServletRequest request, HttpServletResponse response){

        // 验证
        if (StringUtils.isEmpty(uid) || StringUtils.isEmpty(username)){
            return new ResponseUtil<>().setErrorMsg("注册用户名/序号不能为空");
        }

        // 修改数据库 userVerify -- member
        UserVerifyRequest verifyRequest = new UserVerifyRequest();
        verifyRequest.setUserName(username);
        verifyRequest.setUuid(uid);
        UserVerifyResponse verifyResponse = userVerifyService.verify(verifyRequest);

        // 判断一下
        if (verifyResponse.getCode().equals(SysRetCodeConstants.SUCCESS.getCode())){
            return new ResponseUtil<>().setData(null);
        }
        // 失败的话
        return new ResponseUtil<>().setErrorMsg(verifyResponse.getMsg());
    }

}
