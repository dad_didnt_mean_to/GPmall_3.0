package com.cskaoyan.gateway.controller.shopping;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.IProductService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dto.*;
import com.mall.user.annotation.Anoymous;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * @Author: ZaiRiChangYe
 * @Date: 2020/5/13 11:17
 * @Description:商品信息中心控制层
 * 负责查询所有商品，以及商品详情，还有推荐商品
 */
@RestController
@RequestMapping("/shopping")
public class ProductController {

    @Reference
    IProductService productService;

    @GetMapping("/product")
    @Anoymous
    public ResponseData selectProductById(ProductDetailRequest request){
        ProductDetailResponse productDetail = productService.getProductDetail(request);
        ProductDetailDto productDetailDto = productDetail.getProductDetailDto();
        if (productDetailDto == null){
            return new ResponseUtil<>().setErrorMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        return new ResponseUtil<>().setData(productDetailDto);
    }

    @GetMapping("/recommend")
    @Anoymous
    public ResponseData recommendProduct(){
        RecommendResponse recommendGoods = productService.getRecommendGoods();
        List<PanelDto> panelContentItemDtos = recommendGoods.getPanelContentItemDtos();
        if (CollectionUtils.isEmpty(panelContentItemDtos)){
            return new ResponseUtil<>().setErrorMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        return new ResponseUtil<>().setData(panelContentItemDtos);
    }

    @GetMapping("/goods")
    @Anoymous
    public ResponseData queryGoods(AllProductRequest request){
        AllProductResponse allProduct = productService.getAllProduct(request);
        List<ProductDto> productDtoList = allProduct.getProductDtoList();
        if (CollectionUtils.isEmpty(productDtoList)){
            return new ResponseUtil<>().setErrorMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        return new ResponseUtil<>().setData(productDtoList);
    }
}
