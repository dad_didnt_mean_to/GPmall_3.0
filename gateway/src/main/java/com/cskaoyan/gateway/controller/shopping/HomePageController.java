package com.cskaoyan.gateway.controller.shopping;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.IHomeService;
import com.mall.shopping.constants.ShoppingRetCode;

import com.mall.shopping.dto.HomePageResponse;
import com.mall.shopping.dto.PanelDto;
import com.mall.user.annotation.Anoymous;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: ZaiRiChangYe
 * @Date: 2020/5/13 0:16
 * @Description: 主页显示中心控制层
 */
@RestController
@RequestMapping("/shopping")
public class HomePageController {

    @Reference
    IHomeService homeService;

    @GetMapping("/homepage")
    @Anoymous
    public ResponseData homePage(){
        HomePageResponse homepage = homeService.homepage();
        List<PanelDto> panelContentItemDtos = homepage.getPanelContentItemDtos();

        // 如果panelContentItemDtos为空
        if (CollectionUtils.isEmpty(panelContentItemDtos)){
            return new ResponseUtil<>().setErrorMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        return new ResponseUtil<>().setData(panelContentItemDtos);
    }
}
