package com.cskaoyan.gateway.controller.shopping;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.IContentService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dto.NavListResponse;
import com.mall.shopping.dto.PanelContentDto;
import com.mall.user.annotation.Anoymous;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: ZaiRiChangYe
 * @Date: 2020/5/13 10:17
 * @Description: 导航栏显示中心控制层
 */
@RestController
@RequestMapping("/shopping")
public class NavigationController {

    @Reference(check = false)
    private IContentService contentService;

    @GetMapping("navigation")
    @Anoymous
    public ResponseData navigation(){
        NavListResponse navListResponse = contentService.queryNavList();
        List<PanelContentDto> contentDtoList = navListResponse.getPannelContentDtos();
        // 如果contentDtoList为空
        if (CollectionUtils.isEmpty(contentDtoList)){
            return new ResponseUtil<>().setErrorMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        return new ResponseUtil<>().setData(contentDtoList);
    }
}
