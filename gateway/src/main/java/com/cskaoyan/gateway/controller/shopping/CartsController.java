package com.cskaoyan.gateway.controller.shopping;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.ICartService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dto.AddCartRequest;
import com.mall.shopping.dto.AddCartResponse;
import com.mall.user.constants.SysRetCodeConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: ZaiRiChangYe
 * @Date: 2020/5/14 15:45
 * @Description:
 */
@RestController
@RequestMapping("/shopping")
public class CartsController {

    @Autowired
    ICartService cartService;

    /**
     * 加入购物车
     */
    @PostMapping("carts")
    public ResponseData add2Carts(AddCartRequest request){
        AddCartResponse addCartResponse = cartService.addToCart(request);
        request.requestCheck();
        if (addCartResponse == null){
            return new ResponseUtil<>().setErrorMsg(ShoppingRetCode.SYSTEM_TIMEOUT.getMessage());
        }
        return new ResponseUtil<>().setErrorMsg(ShoppingRetCode.SUCCESS.getMessage());
    }
}
