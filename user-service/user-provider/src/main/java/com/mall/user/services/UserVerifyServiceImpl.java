package com.mall.user.services;

import com.mall.user.IUserVerifyService;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.dal.entitys.Member;
import com.mall.user.dal.entitys.UserVerify;
import com.mall.user.dal.persistence.MemberMapper;
import com.mall.user.dal.persistence.UserVerifyMapper;
import com.mall.user.dto.UserVerifyRequest;
import com.mall.user.dto.UserVerifyResponse;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @Author: ZaiRiChangYe
 * @Date: 2020/5/12 12:03
 * @Description: 用户注册激活接口的具体实现类
 */
@Service
@Component
public class UserVerifyServiceImpl implements IUserVerifyService {

    @Autowired
    private UserVerifyMapper userVerifyMapper;

    @Autowired
    private MemberMapper memberMapper;

    @Override
    @Transactional // 兩個upadate操作應該在一個事務當中
    public UserVerifyResponse verify(UserVerifyRequest request) {
        UserVerifyResponse userVerifyResponse = new UserVerifyResponse();
        request.requestCheck();
        Example example = new Example(UserVerify.class);
        example.createCriteria().andEqualTo("uuid",request.getUuid());
        List<UserVerify> userVerifyList = userVerifyMapper.selectByExample(example);

        // 1. 根据uuid去查询userVerify表
        // 首先判断一下
        if (CollectionUtils.isEmpty(userVerifyList)){
            userVerifyResponse.setCode(SysRetCodeConstants.USER_INFOR_INVALID.getCode());
            userVerifyResponse.setMsg(SysRetCodeConstants.USER_INFOR_INVALID.getMessage());
            return userVerifyResponse;
        }
        UserVerify userVerify = userVerifyList.get(0);

        // 2. 把两个username进行比对
        String userName = request.getUserName();
        if (!userName.equals(userVerify.getUsername())){
            userVerifyResponse.setCode(SysRetCodeConstants.USER_INFOR_INVALID.getCode());
            userVerifyResponse.setMsg(SysRetCodeConstants.USER_INFOR_INVALID.getMessage());
            return userVerifyResponse;
        }

        // 3. 若比对成功 -- 修改userVerify表中的激活字段
        userVerify.setIsVerify("Y");
        // 如果需要重写条件 -- 可以清空example -- 进行如下操作
        // example.clear();
        // example.createCriteria().andEqualTo("uuid",request.getUuid());
        int effectedRows = userVerifyMapper.updateByExample(userVerify, example);
        if (effectedRows < 1){
            userVerifyResponse.setCode(SysRetCodeConstants.USER_INFOR_UPDATE_FAIL.getCode());
            userVerifyResponse.setMsg(SysRetCodeConstants.USER_INFOR_UPDATE_FAIL.getMessage());
            return userVerifyResponse;
        }

        // 4. 通过username去查询member表 继续进行修改表中的激活字段
        Example exampleMember = new Example(Member.class);
        exampleMember.createCriteria().andEqualTo("username",request.getUserName());
        List<Member> members = memberMapper.selectByExample(exampleMember);
        if(CollectionUtils.isEmpty(members)){
            userVerifyResponse.setCode(SysRetCodeConstants.USER_INFOR_QUERY_FAIL.getCode());
            userVerifyResponse.setMsg(SysRetCodeConstants.USER_INFOR_QUERY_FAIL.getMessage());
            return userVerifyResponse;
        }

        Member member = members.get(0);
        // 修改为已激活
        member.setIsVerified("Y");
        int effectedRows2 = memberMapper.updateByExample(member, exampleMember);
        if (effectedRows2 < 1){
            userVerifyResponse.setCode(SysRetCodeConstants.USER_INFOR_UPDATE_FAIL.getCode());
            userVerifyResponse.setMsg(SysRetCodeConstants.USER_INFOR_UPDATE_FAIL.getMessage());
            return userVerifyResponse;
        }

        // 以上成功，表示均已成功
        userVerifyResponse.setCode(SysRetCodeConstants.SUCCESS.getCode());
        userVerifyResponse.setMsg(SysRetCodeConstants.SUCCESS.getMessage());
        return userVerifyResponse;
    }
}
