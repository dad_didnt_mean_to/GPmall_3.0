package com.mall.user;

import com.mall.user.dto.UserVerifyRequest;
import com.mall.user.dto.UserVerifyResponse;

/**
 * @Author: ZaiRiChangYe
 * @Date: 2020/5/12 12:00
 * @Description: 用户注册激活接口
 */
public interface IUserVerifyService {

    UserVerifyResponse verify(UserVerifyRequest request);
}
