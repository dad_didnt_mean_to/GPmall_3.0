package com.mall.user;

import com.mall.user.dto.CheckAuthRequest;
import com.mall.user.dto.CheckAuthResponse;
import com.mall.user.dto.UserLoginRequest;
import com.mall.user.dto.UserLoginResponse;

/**
 * @Author: ZaiRiChangYe
 * @Date: 2020/5/11 20:56
 * @Description: 登陆相关服务
 */
public interface ILoginService {

    UserLoginResponse login(UserLoginRequest request);

    /**
     * 验证token的方法
     */
    CheckAuthResponse validToken(CheckAuthRequest checkAuthRequest);
}
