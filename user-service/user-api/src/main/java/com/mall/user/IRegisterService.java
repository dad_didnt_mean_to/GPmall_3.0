package com.mall.user;

import com.mall.user.dto.UserRegisterRequest;
import com.mall.user.dto.UserRegisterResponse;

/**
 * @Author: ZaiRiChangYe
 * @Date: 2020/5/11 15:43
 * @Description: 注册相关服务
 */
public interface IRegisterService {

    /**
     * 用户注册接口
     * 接口文档中显示 -- "result":null -- 这边返回成功信息或者失败信息
     */
    UserRegisterResponse register(UserRegisterRequest registerRequest);
}
