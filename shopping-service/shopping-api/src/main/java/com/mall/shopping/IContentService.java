package com.mall.shopping;

import com.mall.shopping.dto.NavListResponse;


public interface IContentService {
    /**
     * 导航栏显示
     * @return
     */
    NavListResponse queryNavList();
}
