package com.mall.shopping.service;

import com.mall.shopping.IContentService;
import com.mall.shopping.converter.ContentConverter;
import com.mall.shopping.dal.entitys.PanelContent;
import com.mall.shopping.dal.persistence.PanelContentMapper;
import com.mall.shopping.dto.NavListResponse;
import com.mall.shopping.dto.PanelContentDto;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @Author: ZaiRiChangYe
 * @Date: 2020/5/13 10:30
 * @Description: 查询所有 -- 导航栏显示
 */
@Service
@Component
public class ContentServiceImpl implements IContentService {

    @Autowired
    PanelContentMapper contentMapper;

    @Autowired
    ContentConverter contentConverter;

    @Override
    public NavListResponse queryNavList() {

        // 需要返回navListResponse就给他牛一个出来
        NavListResponse navListResponse = new NavListResponse();

        // 去panelconten表中查询出panel_id为0的数据
        Example example = new Example(PanelContent.class);
        example.createCriteria().andEqualTo("panelId",0);
        List<PanelContent> panelContents = contentMapper.selectByExample(example);

        // 转化成PanelContentDto类型
        List<PanelContentDto> panelContentDtos = contentConverter.panelContents2Dto(panelContents);

        // 塞入navListResponse，然后返回
        navListResponse.setPannelContentDtos(panelContentDtos);
        return navListResponse;
    }
}
