package com.mall.shopping.service;

import com.mall.shopping.IHomeService;
import com.mall.shopping.converter.ContentConverter;
import com.mall.shopping.dal.entitys.Item;
import com.mall.shopping.dal.entitys.Panel;
import com.mall.shopping.dal.entitys.PanelContent;
import com.mall.shopping.dal.entitys.PanelContentItem;
import com.mall.shopping.dal.persistence.ItemMapper;
import com.mall.shopping.dal.persistence.PanelContentMapper;
import com.mall.shopping.dal.persistence.PanelMapper;
import com.mall.shopping.dto.HomePageResponse;
import com.mall.shopping.dto.PanelContentItemDto;
import com.mall.shopping.dto.PanelDto;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @Author: ZaiRiChangYe
 * @Date: 2020/5/13 10:28
 * @Description:
 */
@Service
@Component
public class HomeServiceImpl implements IHomeService {

    @Autowired
    PanelMapper panelMapper;

    @Autowired
    ContentConverter converter;

    @Autowired
    PanelContentMapper contentMapper;

    @Autowired
    ItemMapper itemMapper;

    @Override
    public HomePageResponse homepage() {
        // 牛一个response
        HomePageResponse response = new HomePageResponse();
        Example example1 = new Example(Panel.class);
        // 查询position为0的panels
        example1.createCriteria().andEqualTo("position",0);
        List<Panel> panels = panelMapper.selectByExample(example1);
        // 转换为panelDtos
        List<PanelDto> panelDtos = converter.panels2Dto(panels);

        // 挨个轮播
        for (PanelDto panelDto : panelDtos) {
            // 獲取到單個panelId
            Integer panelId = panelDto.getId();
            Example example2 = new Example(PanelContent.class);
            // 來查詢panelContent
            example2.createCriteria().andEqualTo("panelId",panelId);
            List<PanelContent> panelContents = contentMapper.selectByExample(example2);
            // 轉換一下
            List<PanelContentItemDto> itemDtos = converter.panelContents2ItemDto(panelContents);
            for (PanelContentItemDto itemDto : itemDtos) {
                // 获取到productI取到item表中查询商品名称和卖点
                Long productId = itemDto.getProductId();
                Example example3 = new Example(Item.class);
                example3.createCriteria().andEqualTo("id",productId);
                List<Item> items = itemMapper.selectByExample(example3);
                // 塞入
                for (Item item : items) {
                    // 商品名称
                    itemDto.setProductName(item.getTitle());
                    // 卖点
                    itemDto.setSubTitle(item.getSellPoint());
                    // 价格
                    itemDto.setSalePrice(item.getPrice());
                }
            }
            panelDto.setPanelContentItems(itemDtos);
        }
        response.setPanelContentItemDtos(panelDtos);
        return response;
    }
}
