package com.mall.shopping.service;

import com.mall.shopping.IProductCateService;
import com.mall.shopping.converter.ProductCateConverter;
import com.mall.shopping.dal.entitys.ItemCat;
import com.mall.shopping.dal.persistence.ItemCatMapper;
import com.mall.shopping.dto.AllProductCateRequest;
import com.mall.shopping.dto.AllProductCateResponse;
import com.mall.shopping.dto.ProductCateDto;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author: ZaiRiChangYe
 * @Date: 2020/5/13 10:32
 * @Description:
 */
@Service
@Component
public class ProductCateServiceImpl implements IProductCateService {

    @Autowired
    ItemCatMapper catMapper;

    @Autowired
    ProductCateConverter cateConverter;

    @Override
    public AllProductCateResponse getAllProductCate(AllProductCateRequest request) {
        request.requestCheck();
        AllProductCateResponse response = new AllProductCateResponse();
        List<ItemCat> itemCats = catMapper.selectAll();
        List<ProductCateDto> productCateDtos = cateConverter.items2Dto(itemCats);
        response.setProductCateDtoList(productCateDtos);
        return response;
    }
}
