package com.mall.order.biz.handler;

import com.mall.commons.tool.exception.BizException;
import com.mall.commons.tool.utils.NumberUtils;
import com.mall.order.biz.callback.SendEmailCallback;
import com.mall.order.biz.callback.TransCallback;
import com.mall.order.biz.context.CreateOrderContext;
import com.mall.order.biz.context.TransHandlerContext;
import com.mall.order.constant.OrderRetCode;
import com.mall.order.constants.OrderConstants;
import com.mall.order.dal.entitys.Order;
import com.mall.order.dal.entitys.OrderItem;
import com.mall.order.dal.persistence.OrderItemMapper;
import com.mall.order.dal.persistence.OrderMapper;
import com.mall.order.dto.CartProductDto;
import com.mall.order.utils.GlobalIdGeneratorUtil;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 *  ciggar
 * create-date: 2019/8/1-下午5:01
 * 初始化订单 生成订单 -- 数据库中进行插入
 */

@Slf4j
@Component
public class InitOrderHandler extends AbstractTransHandler {


    @Autowired
    OrderMapper orderMapper; // 插入订单表使用

    @Autowired
    OrderItemMapper itemMapper; // 插入订单商品关联表使用

    @Override
    public boolean isAsync() {
        return false;
    }

    @Override
    public boolean handle(TransHandlerContext context) {
        CreateOrderContext orderContext = (CreateOrderContext) context;

        String orderId = UUID.randomUUID().toString();
        Order order = new Order();
        // TODO 全局id生成器(发号器)
        order.setOrderId(orderId);
        order.setUserId(orderContext.getUserId());
        order.setBuyerNick(orderContext.getBuyerNickName());
        order.setPayment(orderContext.getOrderTotal());
        order.setCreateTime(new Date());
        order.setUpdateTime(new Date());
        // 设置订单状态 -- 初始化状态0
        order.setStatus(OrderConstants.ORDER_STATUS_INIT);

        // 1. 插入订单表
        orderMapper.insert(order);

        List<Long> buyProductIds = new ArrayList<>();
        // 2. 插入订单商品关联表
        List<CartProductDto> cartProductDtoList = orderContext.getCartProductDtoList();
        for (CartProductDto cartProductDto : cartProductDtoList) {
            OrderItem orderItem = new OrderItem();
            orderItem.setOrderId(orderId);
            String orderItemId = UUID.randomUUID().toString();
            orderItem.setId(orderItemId);
            Long productId = cartProductDto.getProductId();
            orderItem.setItemId(productId);
            orderItem.setNum(cartProductDto.getProductNum().intValue());
            orderItem.setPrice(cartProductDto.getSalePrice().doubleValue());
            orderItem.setTitle(cartProductDto.getProductName());
            orderItem.setPicPath(cartProductDto.getProductImg());
            //计算总价
            BigDecimal totalFee = cartProductDto.getSalePrice().multiply(new BigDecimal(cartProductDto.getProductNum()));
            orderItem.setTotalFee(totalFee.doubleValue());
            // 库存锁定状态 1库存已锁定
            orderItem.setStatus(1);
            buyProductIds.add(productId);
            itemMapper.insert(orderItem);
        }

        orderContext.setOrderId(orderId);
        orderContext.setBuyProductIds(buyProductIds);
        return true;
    }
}
