package com.mall.order.biz.handler;/**
 * Created by ciggar on 2019/8/1.
 */

import com.mall.commons.tool.exception.BizException;
import com.mall.order.biz.context.CreateOrderContext;
import com.mall.order.biz.context.TransHandlerContext;
import com.mall.order.constant.OrderRetCode;
import com.mall.order.dal.entitys.OrderShipping;
import com.mall.order.dal.persistence.OrderShippingMapper;
import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 *  ciggar
 * create-date: 2019/8/1-下午5:06
 *
 * 处理物流信息（商品寄送的地址）
 */
@Slf4j
@Component
public class LogisticalHandler extends AbstractTransHandler {

    @Autowired
    OrderShippingMapper shippingMapper; // 插入物流信息记录


    @Override
    public boolean isAsync() {
        return false;
    }

    @Override
    public boolean handle(TransHandlerContext context) {
        // 向下转型
        CreateOrderContext orderContext = (CreateOrderContext) context;
        OrderShipping shipping = new OrderShipping();
        shipping.setOrderId(orderContext.getOrderId());
        shipping.setReceiverName(orderContext.getUserName());
        shipping.setReceiverAddress(orderContext.getStreetName());
        shipping.setReceiverPhone(orderContext.getTel());
        shipping.setCreated(new Date());
        shipping.setUpdated(new Date());

        int effectedRows = shippingMapper.insert(shipping);
        // TODO 插入异常处理 -- 有待添加
        if (effectedRows < 1){
            throw new BizException(OrderRetCode.SHIPPING_DB_SAVED_FAILED.getCode(),
                                   OrderRetCode.SHIPPING_DB_SAVED_FAILED.getMessage());
        }
        return true;
    }
}
